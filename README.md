# Cordova Plugin

```json
"cordova-plugin-watchextension": {
    "WATCH_NAME": "AmSmart2Watch",
    "WATCH_BUNDLE_SUFFIX": "watchkit",
    "WATCH_EXTENSION_BUNDLE_SUFFIX": "watchkit.extension",
    "WATCH_FILES_SRC": "apple-watch-extension/src/"
}
```