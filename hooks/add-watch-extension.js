// @ts-check
var elementTree = require('elementtree');
var fs = require('fs');
var path = require('path');
var plist = require('plist');
var Q = require('q');
var xcode = require('xcode');

function log(logString, type) {
  var prefix;
  switch (type) {
    case 'error':
      prefix = ' [ ERROR ] ';
      throw new Error(prefix + logString);
    case 'info':
      prefix = ' [ INFO ] ';
      break;
    case 'success':
      prefix = ' [ DONE ] ';
      break;
  }
  console.log(prefix + logString);
}

function collectFiles(path, callback) {
  if (!fs.existsSync(path)) return;
  let files = fs.readdirSync(path);
  if (files) {
    files.forEach(file => {
      let newPath = `${path}/${file}`;
      let stat = fs.lstatSync(newPath);
      if (stat.isFile()) {
        callback(newPath);
      }
      if (stat.isDirectory()) {
        collectFiles(newPath, callback);
      }
    });
  }
}

function copy(src, files) {
  if (files.length < 1) return;
  log('Copying files...', 'info');
  files.forEach(file => {
    let destination = file.replace(src, 'platforms/ios/');
    let checkDirectory = destination.split('/');
    checkDirectory.pop();
    checkDirectory = checkDirectory.join('/');
    if (!fs.existsSync(checkDirectory)) {
      checkAndCreateDestinationPath(checkDirectory);
    }
    log(destination, 'info');
    fs.copyFileSync(file, destination);
  });
}

function checkAndCreateDestinationPath(fileDestination) {
  const dirPath = fileDestination.split('/');
  dirPath.forEach((element, index) => {
    let path = dirPath.slice(0, index + 1).join('/');
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path);
    }
  });
}

function getPreferenceValueFromPackageJson(config, name) {
  var value = config.match(new RegExp('"' + name + '":\\s"(.*?)"', "i"));
  if (value && value[1]) {
    return value[1];
  } else {
    return null;
  }
};

function getPreferenceValue(config, name) {
  var value = config.match(new RegExp('name="' + name + '" value="(.*?)"', "i"));
  if (value && value[1]) {
    return value[1];
  } else {
    var packageJson = fs.readFileSync("package.json").toString();
    var nameValue = getPreferenceValueFromPackageJson(packageJson, name);
    if (nameValue) {
      return nameValue;
    } else {
      return null;
    }
  }
}

// function replacePlaceholdersInPlist(plistPath, placeHolderValues) {
//   var plistContents = fs.readFileSync(plistPath, 'utf8');
//   for (var i = 0; i < placeHolderValues.length; i++) {
//     var placeHolderValue = placeHolderValues[i],
//       regexp = new RegExp(placeHolderValue.placeHolder, "g");
//     plistContents = plistContents.replace(regexp, placeHolderValue.value);
//   }
//   fs.writeFileSync(plistPath, plistContents);
// }

function getCordovaParameter(variableName, contents) {
  var variable;
  if (process.argv.join("|").indexOf(variableName + "=") > -1) {
    var re = new RegExp(variableName + '=(.*?)(\||$))', 'g');
    variable = process.argv.join("|").match(re)[1];
  } else {
    variable = getPreferenceValue(contents, variableName);
  }
  return variable;
}

log('Running add-watch-extension hook, patching xcode project', 'info');

module.exports = function (context) {
  var deferral = Q.defer();

  if (context.opts.cordova.platforms.indexOf('ios') < 0) {
    log('You have to add the ios platform before adding this plugin!', 'error');
  }

  var contents = fs.readFileSync(path.join(context.opts.projectRoot, 'config.xml'), 'utf-8');

  // Get the plugin variables from the parameters or the config file
  var WATCH_NAME = getCordovaParameter("WATCH_NAME", contents);
  var WATCH_BUNDLE_SUFFIX = getCordovaParameter("WATCH_BUNDLE_SUFFIX", contents);
  var WATCH_EXTENSION_BUNDLE_SUFFIX = getCordovaParameter("WATCH_EXTENSION_BUNDLE_SUFFIX", contents);
  var WATCH_FILES_SRC = getCordovaParameter("WATCH_FILES_SRC", contents);

  if (contents) {
    contents = contents.substring(contents.indexOf('<'));
  }

  // Get the bundle-id from config.xml
  var etree = elementTree.parse(contents);
  var bundleId = etree.getroot().get('id');
  log('Bundle id of your host app: ' + bundleId, 'info');

  var iosFolder = context.opts.cordova.project ? context.opts.cordova.project.root : path.join(context.opts.projectRoot, 'platforms/ios/');
  log('Folder containing your iOS project: ' + iosFolder, 'info');

  fs.readdir(iosFolder, function (err, data) {
    var projectFolder;
    var projectName;

    var run = function () {
      var pbxProject;
      var projectPath;
      projectPath = path.join(projectFolder, 'project.pbxproj');

      log('Parsing existing project at location: ' + projectPath + ' ...', 'info');
      if (context.opts.cordova.project) {
        pbxProject = context.opts.cordova.project.parseProjectFile(context.opts.projectRoot).xcode;
      } else {
        pbxProject = xcode.project(projectPath);
        pbxProject.parseSync();
      }

      var appName = WATCH_NAME;
      var extensionName = WATCH_NAME + " Extension";
      log('Your Watch App will be named: ' + appName, 'info');
      log('Your Watch Extension will be named: ' + extensionName, 'info');

      var widgetBundleId = WATCH_BUNDLE_SUFFIX;
      log('Your widget bundle id will be: ' + bundleId + '.' + widgetBundleId, 'info');

      var appFolder = WATCH_FILES_SRC + appName;
      var extensionFolder = appFolder + " Extension";
      var appSourceFiles = [];
      var appResourceFiles = [];
      var appConfigFiles = [];
      var extensionSourceFiles = [];
      var extensionResourceFiles = [];
      var extensionConfigFiles = [];

      var addBridgingHeader = false;
      var bridgingHeaderName;
      var addXcconfig = false;
      var xcconfigFileName;
      var xcconfigReference;
      var addEntitlementsFile = false;
      var entitlementsFileName;
      var projectPlistPath = path.join(iosFolder, projectName, projectName + '-Info.plist');
      var projectPlistJson = plist.parse(fs.readFileSync(projectPlistPath, 'utf8'));
      // var placeHolderValues = [
      //   {
      //     placeHolder: '__DISPLAY_NAME__',
      //     value: projectPlistJson['CFBundleDisplayName']
      //   },
      //   {
      //     placeHolder: '__APP_IDENTIFIER__',
      //     value: projectPlistJson['CFBundleIdentifier']
      //   },
      //   {
      //     placeHolder: '__BUNDLE_SUFFIX__',
      //     value: widgetBundleId
      //   },
      //   {
      //     placeHolder: '__BUNDLE_SHORT_VERSION_STRING__',
      //     value: projectPlistJson['CFBundleShortVersionString']
      //   },
      //   {
      //     placeHolder: '__BUNDLE_VERSION__',
      //     value: projectPlistJson['CFBundleVersion']
      //   }
      // ];

      collectFiles(appFolder, (file) => {
        var fileExtension = path.extname(file);
        switch (fileExtension) {
          case '.swift':
            appSourceFiles.push(file);
            break;
          case '.plist':
          case '.entitlements':
          case '.xcconfig':
            if (fileExtension === '.plist') {
              // replacePlaceholdersInPlist(path.join(watchForder, file), placeHolderValues);
            }
            if (fileExtension === '.xcconfig') {
              addXcconfig = true;
              xcconfigFileName = file;
            }
            if (fileExtension === '.entitlements') {
              // replacePlaceholdersInPlist(path.join(watchForder, file), placeHolderValues);
              addEntitlementsFile = true;
              entitlementsFileName = file;
            }
            appConfigFiles.push(file);
            break;
          default:
            appResourceFiles.push(file);
            break;
        }
      });
      // collectFiles(extensionFolder, (file) => {
      //   var fileExtension = path.extname(file);
      //   switch (fileExtension) {
      //     case '.swift':
      //       extensionSourceFiles.push(file);
      //       break;
      //     case '.plist':
      //     case '.entitlements':
      //     case '.xcconfig':
      //       if (fileExtension === '.plist') {
      //         // replacePlaceholdersInPlist(path.join(watchForder, file), placeHolderValues);
      //       }
      //       if (fileExtension === '.xcconfig') {
      //         addXcconfig = true;
      //         xcconfigFileName = file;
      //       }
      //       if (fileExtension === '.entitlements') {
      //         // replacePlaceholdersInPlist(path.join(watchForder, file), placeHolderValues);
      //         addEntitlementsFile = true;
      //         entitlementsFileName = file;
      //       }
      //       extensionConfigFiles.push(file);
      //       break;
      //     default:
      //       extensionResourceFiles.push(file);
      //       break;
      //   }
      // });
      copy(WATCH_FILES_SRC, appSourceFiles);
      copy(WATCH_FILES_SRC, appConfigFiles);
      copy(WATCH_FILES_SRC, appResourceFiles);
      copy(WATCH_FILES_SRC, extensionSourceFiles);
      copy(WATCH_FILES_SRC, extensionConfigFiles);
      copy(WATCH_FILES_SRC, extensionResourceFiles);

      // Add PBXNativeTarget to the project
      var apptarget = pbxProject.addTarget(appName, 'watch_app', appName);
      if (apptarget) {
        log('Successfully added PBXNativeTarget!', 'info');
      }

      // Create a separate PBXGroup for the widgets files, name has to be unique and path must be in quotation marks
      var pbxGroupKey = pbxProject.pbxCreateGroup(appName, '"' + appName + '"');
      if (pbxGroupKey) {
        log('Successfully created empty PbxGroup for folder: ' + appName + ' with alias: ' + appName, 'info');
      }

      // Add the PbxGroup to cordovas "CustomTemplate"-group
      var customTemplateKey = pbxProject.findPBXGroupKey({
        name: 'CustomTemplate',
      });
      pbxProject.addToPbxGroup(pbxGroupKey, customTemplateKey);
      log('Successfully added the widgets PbxGroup to cordovas CustomTemplate!', 'info');

      // Add files which are not part of any build phase (config)
      appConfigFiles.forEach(configFile => {
        var file = pbxProject.addFile(configFile.replace(WATCH_FILES_SRC, ''), pbxGroupKey);
        // We need the reference to add the xcconfig to the XCBuildConfiguration as baseConfigurationReference
        if (path.extname(configFile) == '.xcconfig') {
          xcconfigReference = file.fileRef;
        }
      });
      log('Successfully added ' + appConfigFiles.length + ' configuration files!', 'info');

      // Add a new PBXSourcesBuildPhase for our TodayViewController (we can't add it to the existing one because a today extension is kind of an extra app)
      var sourcesBuildPhase = pbxProject.addBuildPhase([], 'PBXSourcesBuildPhase', 'Sources', apptarget.uuid);
      if (sourcesBuildPhase) {
        log('Successfully added PBXSourcesBuildPhase!', 'info');
      }

      // Add a new source file and add it to our PbxGroup and our newly created PBXSourcesBuildPhase
      appSourceFiles.forEach(sourcefile => {
        pbxProject.addSourceFile(sourcefile.replace(WATCH_FILES_SRC, ''), { target: apptarget.uuid }, pbxGroupKey);
      });

      log('Successfully added ' + appSourceFiles.length + ' source files to PbxGroup and PBXSourcesBuildPhase!', 'info');

      // Add a new PBXFrameworksBuildPhase for the Frameworks used by the widget (NotificationCenter.framework, libCordova.a)
      var frameworksBuildPhase = pbxProject.addBuildPhase([], 'PBXFrameworksBuildPhase', 'Frameworks', apptarget.uuid);
      if (frameworksBuildPhase) {
        log('Successfully added PBXFrameworksBuildPhase!', 'info');
      }

      // // Add the frameworks needed by our widget, add them to the existing Frameworks PbxGroup and PBXFrameworksBuildPhase
      // var frameworkFile1 = pbxProject.addFramework(
      //   'NotificationCenter.framework',
      //   { target: target.uuid }
      // );
      // var frameworkFile2 = pbxProject.addFramework('libCordova.a', {
      //   target: target.uuid,
      // }); // seems to work because the first target is built before the second one
      // if (frameworkFile1 && frameworkFile2) {
      //   log('Successfully added frameworks needed by the widget!', 'info');
      // }

      // Add a new PBXResourcesBuildPhase for the Resources used by the widget (MainInterface.storyboard)
      var resourcesBuildPhase = pbxProject.addBuildPhase([], 'PBXResourcesBuildPhase', 'Resources', apptarget.uuid);
      if (resourcesBuildPhase) {
        log('Successfully added PBXResourcesBuildPhase!', 'info');
      }

      //  Add the resource file and include it into the targest PbxResourcesBuildPhase and PbxGroup
      appResourceFiles.forEach(resourcefile => {
        pbxProject.addResourceFile(resourcefile.replace(WATCH_FILES_SRC, ''), { target: apptarget.uuid }, pbxGroupKey);
      });

      //  ----------------------
      //   // Add PBXNativeTarget to the project
      //   var extensiontarget = pbxProject.addTarget(extensionName, 'watch2_extension', extensionName);
      //   if (extensiontarget) {
      //     log('Successfully added PBXNativeTarget!', 'info');
      //   }

      //   // Create a separate PBXGroup for the widgets files, name has to be unique and path must be in quotation marks
      //   var pbxGroupKey = pbxProject.pbxCreateGroup(extensionName, '"' + extensionName + '"');
      //   if (pbxGroupKey) {
      //     log('Successfully created empty PbxGroup for folder: ' + extensionName + ' with alias: ' + extensionName, 'info');
      //   }

      //   // Add the PbxGroup to cordovas "CustomTemplate"-group
      //   var customTemplateKey = pbxProject.findPBXGroupKey({
      //     name: 'CustomTemplate',
      //   });
      //   pbxProject.addToPbxGroup(pbxGroupKey, customTemplateKey);
      //   log('Successfully added the widgets PbxGroup to cordovas CustomTemplate!', 'info');

      //   // Add files which are not part of any build phase (config)
      //   extensionConfigFiles.forEach(configFile => {
      //     var file = pbxProject.addFile(configFile, pbxGroupKey);
      //     // We need the reference to add the xcconfig to the XCBuildConfiguration as baseConfigurationReference
      //     if (path.extname(configFile) == '.xcconfig') {
      //       xcconfigReference = file.fileRef;
      //     }
      //   });
      //   log('Successfully added ' + extensionConfigFiles.length + ' configuration files!', 'info');

      //   // Add a new PBXSourcesBuildPhase for our TodayViewController (we can't add it to the existing one because a today extension is kind of an extra app)
      //   var sourcesBuildPhase = pbxProject.addBuildPhase([], 'PBXSourcesBuildPhase', 'Sources', extensiontarget.uuid);
      //   if (sourcesBuildPhase) {
      //     log('Successfully added PBXSourcesBuildPhase!', 'info');
      //   }

      //   // Add a new source file and add it to our PbxGroup and our newly created PBXSourcesBuildPhase
      //   extensionSourceFiles.forEach(sourcefile => {
      //     pbxProject.addSourceFile(sourcefile, { target: extensiontarget.uuid }, pbxGroupKey);
      //   });

      //   log('Successfully added ' + extensionSourceFiles.length + ' source files to PbxGroup and PBXSourcesBuildPhase!', 'info');

      //   // Add a new PBXFrameworksBuildPhase for the Frameworks used by the widget (NotificationCenter.framework, libCordova.a)
      //   var frameworksBuildPhase = pbxProject.addBuildPhase([], 'PBXFrameworksBuildPhase', 'Frameworks', extensiontarget.uuid);
      //   if (frameworksBuildPhase) {
      //     log('Successfully added PBXFrameworksBuildPhase!', 'info');
      //   }

      //   // // Add the frameworks needed by our widget, add them to the existing Frameworks PbxGroup and PBXFrameworksBuildPhase
      //   // var frameworkFile1 = pbxProject.addFramework(
      //   //   'NotificationCenter.framework',
      //   //   { target: target.uuid }
      //   // );
      //   // var frameworkFile2 = pbxProject.addFramework('libCordova.a', {
      //   //   target: target.uuid,
      //   // }); // seems to work because the first target is built before the second one
      //   // if (frameworkFile1 && frameworkFile2) {
      //   //   log('Successfully added frameworks needed by the widget!', 'info');
      //   // }

      //   // Add a new PBXResourcesBuildPhase for the Resources used by the widget (MainInterface.storyboard)
      //   var resourcesBuildPhase = pbxProject.addBuildPhase([], 'PBXResourcesBuildPhase', 'Resources', extensiontarget.uuid);
      //   if (resourcesBuildPhase) {
      //     log('Successfully added PBXResourcesBuildPhase!', 'info');
      //   }

      //   //  Add the resource file and include it into the targest PbxResourcesBuildPhase and PbxGroup
      //   extensionResourceFiles.forEach(resourcefile => {
      //     pbxProject.addResourceFile(resourcefile, { target: extensiontarget.uuid }, pbxGroupKey);
      //   });

      //   log('Successfully added ' + extensionResourceFiles.length + ' resource files!', 'info');


      // --------------------

      // Add build settings for Swift support, bridging header and xcconfig files
      var configurations = pbxProject.pbxXCBuildConfigurationSection();
      for (var key in configurations) {
        if (typeof configurations[key].buildSettings !== 'undefined') {
          var buildSettingsObj = configurations[key].buildSettings;
          if (typeof buildSettingsObj['PRODUCT_NAME'] !== 'undefined') {
            var productName = buildSettingsObj['PRODUCT_NAME'];
            if (productName.indexOf(appName) >= 0) {
              if (addXcconfig) {
                configurations[key].baseConfigurationReference =
                  xcconfigReference + ' /* ' + xcconfigFileName + ' */';
                log('Added xcconfig file reference to build settings!', 'info');
              }
              if (addEntitlementsFile) {
                buildSettingsObj['CODE_SIGN_ENTITLEMENTS'] = '"' + appName + '/' + entitlementsFileName + '"';
                log('Added entitlements file reference to build settings!', 'info');
              }
              if (addBridgingHeader) {
                buildSettingsObj['SWIFT_OBJC_BRIDGING_HEADER'] =
                  '"$(PROJECT_DIR)/' +
                  appName +
                  '/' +
                  bridgingHeaderName +
                  '"';
                log('Added bridging header reference to build settings!', 'info');
              }
            }
          }
        }
      }

      // Write the modified project back to disc
      log('Writing the modified project back to disk ...', 'info');
      fs.writeFileSync(projectPath, pbxProject.writeSync());
      log('Added app extension to ' + projectName + ' xcode project', 'success');
      deferral.resolve();
    };

    if (err) {
      log(err, 'error');
    }

    if (data && data.length) {
      data.forEach(function (folder) {
        if (folder.match(/\.xcodeproj$/)) {
          projectFolder = path.join(iosFolder, folder);
          projectName = path.basename(folder, '.xcodeproj');
        }
      });
    }

    if (!projectFolder || !projectName) {
      log('Could not find an *.xcodeproj folder in: ' + iosFolder, 'error');
    }

    run();
  });

  return deferral.promise;
};
